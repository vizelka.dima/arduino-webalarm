# Arduino WebAlarm
## Funkcni Pozadavky:
- Nastaveni aktualniho casu
- Nastaveni notifikace (budiku) na konkretni cas
- Zobrazeni vsech nastavenych notifikaci (budiku)
- Zruseni konkretni (budouci) notifikace

## Hardware:
- Deska: WeMos D1 s čipem ESP8226 (WIFI)
- WIFI module
- RTC module (modul realneho casu) (zs-042)  (library: DS3231)
- LCD display
- Senzor teploměru (LM335) + 2K-OHM resistor
- Piezomenic
- Tlačítko na vypnutí vyzvaneni
- SD karta (je soucasti RTC modulu - AT24C32 EEPROM)

## Komunikace
Vymena dat mezi webovou aplikaci a arduinem je ve formatu JSON. Serializaci/Deserializaci prenasenych dat zajisti knihovna ArduinoJson.


## Webova aplikace vytvorena dle navrhu:
https://www.figma.com/file/LVnmcyV5kGA0JcMZXxEsDr/Untitled?node-id=0%3A1

