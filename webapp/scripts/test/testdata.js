const testAlarms = {
    'alarms' : [
        {
            id: 1,
            hour: 13,
            minute: 13,
            dows: [0, 1],
            isEnabled: true
        },
        {
            id: 2,
            hour: 13,
            minute: 13,
            dows: [0, 1, 2, 3, 4, 5, 6],
            isEnabled: true
        },
        {
            id: 3,
            hour: 13,
            minute: 13,
            dows: [2],
            isEnabled: false
        },
        {
            id: 4,
            hour: 13,
            minute: 13,
            dows: [4],
            isEnabled: false
        },
        {
            id: 5,
            hour: 13,
            minute: 13,
            dows: [6],
            isEnabled: true
        },
        {
            id: 6,
            hour: 14,
            minute: 00,
            dows: [3, 1],
            isEnabled: false
        }
    ]
}



        // {
        //     id: 7,
        //     hour: 14,
        //     minute: 00,
        //     dows: [5, 4],
        //     isEnabled: false
        // }
const testDDT = {
    day: 13,
    month: 12,
    year: 2013,
    dow: 0,
    hour: 13,
    minute: 13,
    second: 13
}

