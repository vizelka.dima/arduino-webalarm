class Context{
    constructor(elem) {
        this._elem = elem

        const contextClose = elem.querySelector('.context-close-btn')
        contextClose.addEventListener('click', (e) => {
            this.close()
        })
        this._closer = contextClose
    }

    open (closable = true) {
        if(closable) {
            this._closer.classList.remove('hidden')
        }
        else
        {
            this._closer.classList.add('hidden')
        }
        this._elem.classList.remove('hidden')
    }

    close () {
        this._elem.classList.add('hidden')
    }
}

class AlarmEditContext extends Context {
    constructor(elem)  {
        super(elem)

        const hourE = document.createElement('input')
        hourE.type = 'number'
        hourE.min = 0
        hourE.max = 23

        const hourLabel = document.createElement('label')
        hourLabel.classList.add('input-title')
        hourLabel.textContent = 'Hour'
        hourLabel.append(hourE)

        const minuteE = document.createElement('input')
        minuteE.type = 'number'
        minuteE.min = 0
        minuteE.max = 60

        const minuteLabel = document.createElement('label')
        minuteLabel.classList.add('input-title')
        minuteLabel.textContent = 'Minute'
        minuteLabel.append(minuteE)

        this.hourE = hourE
        this.minuteE = minuteE
        this.dowsE = []
       
        this.id = null

        const formE = document.createElement('form')

        
        formE.append(hourLabel, minuteLabel)

        const dowsContainer = document.createElement('div')
        dowsContainer.classList.add('dows-container')
        dowsContainer.innerHTML = `<div class="input-title">
                                        Days of week
                                    </div>`

        const dowsList = document.createElement('ol')
        dowsList.classList.add('dows')
        for (let i = 1; i <= 7; i++) {
            const li = document.createElement('li')
            const di = document.createElement('input')
            di.type = 'checkbox'
            di.id = `dow-${i}`

            const dlabel = document.createElement('label')
            dlabel.setAttribute('for',  `dow-${i}`) 
            dlabel.textContent = i

            li.append(di, dlabel)
            dowsList.appendChild(li)

            this.dowsE.push(di)
        }
        dowsContainer.appendChild(dowsList)
        formE.appendChild(dowsContainer)

        const card = elem.querySelector('.context-card')
        card.append(formE)
        
        this.errorMsg = document.createElement('span')
        card.append(this.errorMsg)

        const btn = document.createElement('button')
        btn.type = 'submit'
        btn.textContent = 'Save'
        formE.appendChild(btn)

        this.formE = formE
    }

    customize({id, hour, minute, dows}) {
        this.id = id

        this.dowsE.forEach(d => {
            d.checked = false
        })

        dows.forEach(d => {
            this.dowsE[d].checked = true
        })

        this.hourE.value = hour
        this.minuteE.value = minute

        this.hideError()
    }  
    
    getSelected() {
        const hour = +(this.hourE.value)
        const minute = +(this.minuteE.value)
        const dows = []
        this.dowsE.forEach((d,i) => {if(d.checked) {dows.push(i)}} )
        const id = this.id
        
        return {id, hour, minute, dows}
    }

    showError(msg) {
        this.errorMsg.textContent = msg
    }

    hideError() {
        this.errorMsg.textContent = ""
    }
}
  

class DateTimeEditContext extends Context {
    constructor(elem) {
        super(elem)

        this.valueHolders = {
            hour: document.getElementById('edit-hour-input'),
            minute: document.getElementById('edit-minute-input'),
            second: document.getElementById('edit-second-input'),
            day: document.getElementById('edit-day-input'),
            month: document.getElementById('edit-month-input'),
            year: document.getElementById('edit-year-input')
        }
    }


    customize({hour, minute, second, day, month, year}) {
        this.valueHolders.hour.value = hour
        this.valueHolders.minute.value = minute
        this.valueHolders.second.value = second
        this.valueHolders.day.value = day
        this.valueHolders.month.value = month
        this.valueHolders.year.value = year
    }  

    getSelected() {
        const hour = +(this.valueHolders.hour.value) 
        const minute = +(this.valueHolders.minute.value)
        const second = +(this.valueHolders.second.value)
        const day = +(this.valueHolders.day.value)
        const month = +(this.valueHolders.month.value)
        const year = +(this.valueHolders.year.value)
        
        return {hour, minute, second, day, month, year}
    }
}


class IpChangeContext extends Context{
    constructor(elem) {
        super(elem)

        this._ipInput = document.getElementById('edit-ip-input')
        this._saveForm = elem.querySelector('form')
    }

    customize(ip) {
        this._ipInput.value = ip
    }

    getSelected() {
        return this._ipInput.value
    }

    addOnSaveHandler(handler) {
        this._saveForm.addEventListener('submit', handler)
    }
}

class AlarmUI {
    constructor({id, hour, minute, dows, isEnabled}, adderContext, switchCb) {
        this.alarmVals = {id, hour, minute, dows, isEnabled}

        const li = document.createElement('li')
        this.li = li

        li.classList.add('alarm')
        this._toggle(isEnabled)

        const div = document.createElement('div')
        const divDowTime = document.createElement('div')


        const context = document.getElementById('alarm-save-context')

        
        divDowTime.addEventListener('click', (e) => {
            context.classList.remove('hidden')
            adderContext.customize(this.alarmVals)
        })
        
        divDowTime.classList.add('dowtime')
        this.divDowTime = divDowTime
        this.edit({hour, minute, dows})

        const enableSwitchDiv = document.createElement('div')
        enableSwitchDiv.classList.add('enable-switch')
        const enableSwitchLabel = document.createElement('label')
        enableSwitchLabel.classList.add('switch')
        const enableSwitchInput = document.createElement('input')
        enableSwitchInput.type = 'checkbox'
        enableSwitchInput.checked = isEnabled
        const enableSwitchSpan = document.createElement('span')
        enableSwitchSpan.classList.add('slider', 'round')
                    
        enableSwitchLabel.appendChild(enableSwitchInput)
        enableSwitchLabel.appendChild(enableSwitchSpan)
        enableSwitchDiv.appendChild(enableSwitchLabel)

        this.enableSwitch = enableSwitchInput
        enableSwitchInput.addEventListener('change', (e) => {
            e.stopPropagation()
            e.preventDefault()
            enableSwitchInput.checked = !(enableSwitchInput.checked)
            switchCb(this, id, e.target.checked)
        })
        div.appendChild(divDowTime)
        div.appendChild(enableSwitchDiv)
        li.appendChild(div)

    }

    toggle(isEnabled) {
        this.enableSwitch.checked = isEnabled
        this._toggle(isEnabled)
    }

    _toggle(isEnabled) {
        if(isEnabled){
            this._enable()
        }else{
            this._disable()
        }
    }

    _disable() {
        this.alarmVals.isEnabled = false
        this.li.classList.remove('alarm-enabled')
    }

    _enable() {
        this.alarmVals.isEnabled = true
        this.li.classList.add('alarm-enabled')
    }

    edit({hour, minute, dows}) {
        this.alarmVals.hour = hour
        this.alarmVals.minute = minute
        this.alarmVals.dows = dows

        this.divDowTime.innerHTML = `<div class="time">
                                        ${twoDigits(hour)}:${twoDigits(minute)}
                                    </div>
                                    <div class="dows">
                                        ${
                                            dows.length == 7 ? "every day" : dows.map(toShrtDow).join(', ')   
                                        }
                                    </div>`
    }
}

class AlarmListUI {
    constructor(holder, adder, adderContext, switchCb) {
        this.list = []
        this.holder = holder
        this.adderCtx = adderContext
        this.switchCb = switchCb

        this._adder = adder
    }

    load(alarms) {
        this.holder.innerHTML = ''
        this.list = []
        this.holder.prepend(this._adder)

        const wrapper = document.createDocumentFragment()

        alarms.forEach(
            alarm => {
                const alarmUI = new AlarmUI(alarm, this.adderCtx, this.switchCb)
                this.list.push({id: alarm.id, ui: alarmUI})
                wrapper.appendChild(alarmUI.li)
            }
        )
        this.holder.prepend(wrapper)
    }

    add(alarm) {
        const alarmUI = new AlarmUI(alarm, this.adderCtx, this.switchCb)
        this.holder.prepend(alarmUI.li)
        this.list.push({id: alarm.id, ui: alarmUI})
    }

    remove(alarm) {
        this.list = this.list.filter(x => {
            if(x.id === alarm.id){
                this.holder.remove(alarm.ui.li)
                return false
            }
            return true
        })   
    }

    toggleById(id, enabled) {
        const found = this.list.find(x => x.id === id)
        found.ui.toggle(enabled)
    }

    editById({id, hour, minute, dows}) {
        const found = this.list.find(x => x.id === id)

        found.ui.edit({hour, minute, dows})
    }
}
