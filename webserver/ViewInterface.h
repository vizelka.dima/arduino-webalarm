#pragma once

struct TimeView
{
  int second;
  int minute;
  int hour;
};


struct DateView
{
  int dow;
  int day;
  int month;
  int year;
};

struct TemperatureView
{
  float temperature;
};



enum Dow
{
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
};


class ViewInterface
{
  public:
  virtual void init             ()                                {}

  virtual void show             (const char * txt)                = 0;
  
  virtual void showTime         (TimeView timeView)               = 0;
  
  virtual void showDate         (DateView dateView)               = 0;
  
  virtual void showDateTime     (TimeView timeView, 
                                 DateView dateView)               = 0;
                                
  virtual void showTemperature  (TemperatureView temperatureView) = 0;  

  virtual void clear            ()                                = 0;
};      
