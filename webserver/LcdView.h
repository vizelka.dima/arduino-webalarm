#pragma once

#include <LiquidCrystal_I2C.h>
#include "ViewInterface.h"


class LcdView : public ViewInterface
{
  public:
    LcdView(LiquidCrystal_I2C & lcdInstance) : _lcd(lcdInstance),
                                               lastTime(TimeView{-1, -1, -1}),
                                               lastDate(DateView{-1, -1, -1, -1}),
                                               lastTemperature(TemperatureView{0}),
                                               timeDelimsVisible(false),
                                               dateDelimsVisible(false)
    {}

    void init             ();

    void show             (const char * txt);
    
    void showTime         (TimeView timeView);
    
    void showDate         (DateView dateView);
    
    void showDateTime     (TimeView timeView,
                           DateView dateView);
                                  
    void showTemperature  (TemperatureView temperatureView);
    
    void clear            ();

  private:
    LiquidCrystal_I2C& _lcd;

    TimeView lastTime {};

    DateView lastDate {};

    TemperatureView lastTemperature {};

    bool timeDelimsVisible;
    
    bool dateDelimsVisible;
    
    int numLen            (int num);
};
