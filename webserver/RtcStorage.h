#pragma once

#include <DS3231.h>
#include <AT24Cxx.h>
#include "StorageInterface.h"

class RtcStorage : public StorageInterface 
{
  public:
                 RtcStorage       (AT24Cxx & eep) : _addr(0), 
                                                    _eep(eep),
                                                    freeMemSize(0),
                                                    needUpdateNext(true) {}

    void         clearMem         ();
    
    void         init             ();

    const mAlarmList&   getAlarms        ();

    int          saveAlarm        (mAlarm alarm);  // false if not enough space available
    
    bool         removeAlarm      (int id);

    bool         validateAlarm    (const mAlarm &alarm);
    
    bool         existsById       (int id);
    
    bool         disableAlarm     (int id);
    
    bool         enableAlarm      (int id);
  
    bool         changeAlarmDows  (int id, 
                                   bool dows[7]);

    bool         editAlarm        (int id, 
                                   mAlarm newAlarm);

    bool         changeAlarmHour  (int id, int hour);

    bool         changeAlarmMinute(int id, int minute);
  
    mAlarm       getNextAlarm     (int hour, 
                                   int minute, 
                                   int dow);


    void         updateNext       ();
    
  private:
    int          alarmDistance    (int hour, int minute, int dow, 
                                   int hn, int mn, int dn);

    bool         validateMinSec   (int minSec);

    bool         validateHour     (int hour);
    
    AT24Cxx& _eep;

    const int MAX_ALARMS_CNT = 8;
    const int MEM_FLAG_ADDR = 0;
    
    mAlarmList alarms;
    int freeMem[8] {};
    int freeMemSize;
    
    byte memFlag;
    
    byte dowActiveFlag;
    byte hour;
    byte minute;

    int _addr;

    mAlarm nextCached {};
    bool needUpdateNext;

    byte twoPows[8] = {1, 2, 4, 8, 16, 32, 64, 128};
    
    byte        myTwoPow(int p);

    bool        validateId(int id);
};
