#include "RtcStorage.h"

byte RtcStorage::myTwoPow(int p)
{
  return this->twoPows[p % 8]; 
}

bool RtcStorage::validateId(int id)
{
  return (id >= 0 && id < this->MAX_ALARMS_CNT);
}

bool RtcStorage::validateHour(int hour)
{
    return (hour >= 0 && hour < 24);
}

bool RtcStorage::validateMinSec(int minSec)
{
    return (minSec >= 0 && minSec < 60);
}
  
  
bool RtcStorage::validateAlarm(const mAlarm & alarm)
{
  return (validateHour(alarm.hour) && validateMinSec(alarm.minute));
}

void RtcStorage::clearMem()
{
  this->_eep.write(this->MEM_FLAG_ADDR, 0xFF);
  this->memFlag = 0xFF;
  this->freeMemSize = 8;

  for(int i = 0; i < 8; ++i)
  {
    this->alarms.arr[i] = mAlarm {-1, -1};
  }

  this->needUpdateNext = true;
}

void         RtcStorage::init             ()
{
  this->memFlag = this->_eep.read(this->MEM_FLAG_ADDR);
  this->freeMemSize = 0;
  byte mem = this->memFlag;
  
  for(int i = 0; i < 8; ++i)
  {
    bool isFree = (mem >> i) & 1;
    
    if(isFree)
    {
      this->freeMem[(this->freeMemSize)++] = i;
      this->alarms.arr[i] = mAlarm {-1, -1};
    }
    else
    {
      int offset = (i + 1) * 3;
      
      this->alarms.arr[i].hour   = this->_eep.read(offset);
      this->alarms.arr[i].minute = this->_eep.read(offset + 1);
      
      byte dowEnabledFlag = this->_eep.read(offset + 2);
      
      this->alarms.arr[i].isEnabled = dowEnabledFlag & 1;

      for(int j = 1; j <= 7; ++j)
      {
        this->alarms.arr[i].dows[j - 1] = (dowEnabledFlag >> j) & 1;    
      }
    }

  }
}


const mAlarmList&   RtcStorage::getAlarms        ()
{
  return this->alarms;
}



int RtcStorage::alarmDistance(int hour, int minute, int dow, int hn, int mn, int dn) 
{
  int alarm = dow * 24 * 60 + hour * 60 + minute;
  int now = dn * 24 * 60 + hn * 60 + mn;
  
  int d = (alarm - now);
  return d >= 0 ? d : (d + (7 * 24 * 60));
}


mAlarm RtcStorage::getNextAlarm    (int hour, 
                                    int minute, 
                                    int dow) 
{
  if(!(this->needUpdateNext)){
    return this->nextCached;
  }
    
  mAlarm next {-1};

  int minDistance = (7 * 24 * 60) + 1;
  for(int i = dow; i < dow + 7; ++i)
  {    
  
    int d = i % 7;
    bool existsForDow = false;
    for(int a = 0; a < 8; ++a)
    {
    
      mAlarm it = this->alarms.arr[a];
      if(it.hour == -1 || it.dows[d] == false)
      {
       continue;
      }
      int itDistance = this->alarmDistance(it.hour, it.minute, d, hour, minute, dow); 
      
      if(next.hour == -1 && it.hour != -1 && it.isEnabled)
      { 
        minDistance = itDistance;
        existsForDow = true;
        next = it;
        continue;
      }

      
      if(it.isEnabled && itDistance < minDistance)
      {
        minDistance = itDistance;
        existsForDow = true;
        next = it;
        
        continue;
      }
      
    }

    if(existsForDow && (i > dow || (i == dow && minDistance < 24 * 60)))
    {
      break;  
    }
  }

  
  this->needUpdateNext = false;
  this->nextCached = next;

  return next;
}


bool        RtcStorage::existsById         (int id)
{
  if(!(this->validateId(id)))
  {
    return false;
  }

  return (this->alarms.arr[id].hour != -1);
}
    
int         RtcStorage::saveAlarm        (mAlarm alarm)
{
  if(this->freeMemSize <= 0)
  {
    return -1;
  }

  if(!(this->validateAlarm(alarm)))
  {
    return -2;
  }

  this->needUpdateNext = true;
  
  int freeAlarmId = this->freeMem[--(this->freeMemSize)];
  int freeAddr = (freeAlarmId + 1) * 3;



  this->memFlag &= ((this->myTwoPow(freeAlarmId)) ^ 0xFF);
  this->_eep.write(this->MEM_FLAG_ADDR, this->memFlag);
  
  this->_eep.write(freeAddr, alarm.hour);
  this->_eep.write(freeAddr + 1, alarm.minute);

  byte dowEnabledFlag = alarm.isEnabled;
  
  for(int i = 1; i <= 7; ++i)
  {
    if(alarm.dows[i - 1])
    {
       dowEnabledFlag |= (this->myTwoPow(i));
    }
  }

  this->_eep.write(freeAddr + 2, dowEnabledFlag);
  
  this->alarms.arr[freeAlarmId] = alarm;

  return freeAlarmId;
}

bool         RtcStorage::removeAlarm      (int id)
{
  if(!(this->existsById(id)))
  {
    return false;
  }
  
  this->needUpdateNext = true;
  
  this->alarms.arr[id].hour = -1;
  this->freeMem[(this->freeMemSize)++] = id;
  this->memFlag |= (this->myTwoPow(id));  
  this->_eep.write(this->MEM_FLAG_ADDR, this->memFlag);

  return true;
}

bool         RtcStorage::disableAlarm     (int id)
{
  if(!(this->existsById(id)))
  {
    return false;
  }
 
  this->needUpdateNext = true;
  
  this->alarms.arr[id].isEnabled = false;

  int alarmAddr = (id + 1) * 3;

  byte dowEnabledFlag = this->_eep.read(alarmAddr + 2);
  dowEnabledFlag &= 0xFE; 
  
  this->_eep.write(alarmAddr + 2, dowEnabledFlag);

  return true;
}

bool         RtcStorage::enableAlarm      (int id)
{
  if(!(this->existsById(id)))
  {
    return false;
  }
 
  this->needUpdateNext = true;
  
  this->alarms.arr[id].isEnabled = true;

  int alarmAddr = (id + 1) * 3;

  byte dowEnabledFlag = this->_eep.read(alarmAddr + 2);
  dowEnabledFlag |= 1; 
  
  this->_eep.write(alarmAddr + 2, dowEnabledFlag);

  return true;
}

bool         RtcStorage::changeAlarmDows  (int id, 
                                           bool dows[7])
{
  if(!(this->existsById(id)))
  {
    return false;
  }
 
  this->needUpdateNext = true;
  
  mAlarm alarm = this->alarms.arr[id];
  
  byte dowEnabledFlag = alarm.isEnabled;
  
  for(int i = 1; i <= 7; ++i)
  {
    if(dows[i - 1])
    {
       dowEnabledFlag |= (this->myTwoPow(i));    
    }
    this->alarms.arr[id].dows[i - 1] = dows[i - 1];
  }

  int alarmAddr = (id + 1) * 3;

  this->_eep.write(alarmAddr + 2, dowEnabledFlag);

  return true;
}

bool         RtcStorage::editAlarm        (int id, 
                                           mAlarm newAlarm)
{
  if(!(this->existsById(id)) || 
      !(this->validateAlarm(newAlarm)))
  {
    return false;
  }
 
  if(!(this->changeAlarmDows(id, newAlarm.dows)))
  {
    return false;  
  }

  int alarmAddr = (id + 1) * 3;

  if(this->alarms.arr[id].isEnabled != newAlarm.isEnabled)
  {
      this->needUpdateNext = true;
    
     if(newAlarm.isEnabled)
      {
        this->enableAlarm(id);
      }
      else
      {
        this->disableAlarm(id);
      }
  }
 
   
  if(this->alarms.arr[id].hour != newAlarm.hour)
  {
    this->needUpdateNext = true;
    
    this->alarms.arr[id].hour = newAlarm.hour;
    this->_eep.write(alarmAddr, newAlarm.hour);
  }

  if(this->alarms.arr[id].minute != newAlarm.minute)
  {
    this->needUpdateNext = true;
    
    this->alarms.arr[id].minute = newAlarm.minute;
    this->_eep.write(alarmAddr + 1, newAlarm.minute);
  }

  return true;
}


bool         RtcStorage::changeAlarmHour  (int id, int hour)
{
  if(!(this->existsById(id)) || !(this->validateHour(hour)))
  {
    return false;
  }
 
  if(this->alarms.arr[id].hour != hour)
  {
    this->needUpdateNext = true;
    
    int alarmAddr = (id + 1) * 3;
    this->alarms.arr[id].hour = hour;
    this->_eep.write(alarmAddr, hour);
  }

  return true;
}

bool         RtcStorage::changeAlarmMinute(int id, int minute)
{
 if(!(this->existsById(id)) || !(this->validateMinSec(minute)))
 {
    return false;
 }
 
 if(this->alarms.arr[id].minute != minute)
  {
    this->needUpdateNext = true;
    
    int alarmAddr = (id + 1) * 3;
    this->alarms.arr[id].minute = minute;
    this->_eep.write(alarmAddr + 1, minute);
  }  

  return true;
}


void        RtcStorage::updateNext()
{
  this->needUpdateNext = true;  
}
