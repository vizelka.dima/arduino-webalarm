#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiManager.h>

#include <LiquidCrystal_I2C.h>

#include <DS3231.h>
#include <Wire.h>
#include <AT24Cxx.h>

#include <ArduinoJson.h>
#include <uri/UriRegex.h>

#include "LcdView.h"
#include "RtcTemperature.h"
#include "RtcDateTime.h"
#include "RtcStorage.h"


// ========== CONFIGS ==========================================
#define i2c_address 0x57
#define BTN_PIN D13
#define PIEZO_PIN D9

// ========== [GLOBALS] APP DEPENDENCIES/COMPONENTS ============
LiquidCrystal_I2C lcd(0x27,16,2); // set the LCD address to 0x27 for a 16 chars and 2 line display
ViewInterface && _view = LcdView(lcd);  

DS3231 rtc;
AT24Cxx eep(i2c_address, 32);

TemperatureInterface &&  _temperature  = RtcTemperature(rtc);
DateTimeInterface &&     _clk          = RtcDateTime(rtc);
StorageInterface &&      _storage      = RtcStorage(eep);


// =========== WEBSERVER =======================================
ESP8266WebServer server;
MDNSResponder mdns;
WiFiManager wm;

bool connectWifi()
{
  WiFi.mode(WIFI_STA);
  
  //reset saved settings - dev mode
//  wm.resetSettings();
  
  wm.setConfigPortalBlocking(false);
  wm.setConfigPortalTimeout(180);

  bool wifiConn = wm.autoConnect("WebClock", "password1234");
  if(wifiConn){
    Serial.println("connected :)");
  }
  else 
  {
    Serial.println("non blocking config portal running");
  }

//  wm.startConfigPortal("WebClock");

  Serial.println(WiFi.localIP());
  

//return false;
  return wifiConn;
}



bool checkWifi()
{
  return (WiFi.status() == WL_CONNECTED);
}

void setCrossOrigin(){
    server.sendHeader(F("Access-Control-Allow-Methods"), F("PUT,POST,PATCH,DELETE,GET,OPTIONS"));
    server.sendHeader(F("Access-Control-Allow-Headers"), F("*"));
};


void sendCrossOriginHeader(){
    Serial.println(F("sendCORSHeader"));
    server.sendHeader(F("access-control-allow-credentials"), F("false"));
    setCrossOrigin();
    server.send(204);
}

void handleNotFound(){
  String message = "Page Not Found";
  message += "URI: ";
  message += server.uri();
  message += "nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "nArguments: ";
  message += server.args();
  message += "n";
  
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "n";
  }
  
  server.send(404, "text/plain", message);
}


void handleRoot()
{
  server.send(200, F("text/plain"), "WELCOME TO CLOCK API :)");  
}

void getIpAddress()
{
  if(!checkWifi())
  {
    server.send(404);
    return;
  }

  StaticJsonDocument<256> ipJson;
  ipJson["ip"] = WiFi.localIP().toString();
  
  char resp[256];
  serializeJson(ipJson, resp);
  
  server.send(200, F("application/json"), resp);
}



void addApiRouting()
{
  //GET - DATETIME
  server.on("/datetime", HTTP_GET, getDateTime);

  //PUT - DATETIME
  server.on("/datetime", HTTP_PUT, setDateTime);
  
  //GET - ALARMS
  server.on("/alarms", HTTP_GET, getAllAlarms);

  //POST - ALARMS 
  server.on("/alarms", HTTP_POST, postAlarm);

  //DELETE - ALARMS/{id}
  server.on(UriRegex("/alarms/([0-9]+)"), HTTP_DELETE, removeAlarm);

  //PATCH - ALARMS/{id} 
  server.on(UriRegex("/alarms/([0-9]+)"), HTTP_PATCH, editAlarm);

  //INIT - CLEAR MEMORY
  server.on("/clear", HTTP_GET, deleteAllAlarms);
  
  //OPTIONS - CORS POLICY HEADERS FOR - POST|PUT|PATCH|DELETE
  server.on(F("/datetime"), HTTP_OPTIONS, sendCrossOriginHeader);

  server.on(F("/alarms"), HTTP_OPTIONS, sendCrossOriginHeader);

  server.on(F("/clear"), HTTP_OPTIONS, sendCrossOriginHeader);
  
  server.on(UriRegex("/alarms/([0-9]+)"), HTTP_OPTIONS, sendCrossOriginHeader);
}


bool serverStarted = false;
void startServer()
{
  if(!serverStarted && checkWifi())
  {
    if (mdns.begin("webclock", WiFi.localIP())) {
      Serial.println("MDNS responder started");
    }
    
    server.on("/", handleRoot);
    server.on("/ip", getIpAddress);
    server.onNotFound(handleNotFound);
    
    addApiRouting();
    server.enableCORS(true);
  
    
    server.begin();
    
    Serial.println("HTTP server started");
    serverStarted = true; 
  }
  
}



// =========== REST API CONTROLLERS ==========================================
// ---------------------
// --- GET '/alarms' ---
// ---------------------
char cachedAlarmResp[1024] {};
bool alarmsChanged = true;

void getAllAlarms()
{
  if(!alarmsChanged)
  {
    server.send(200, F("application/json"), cachedAlarmResp);
    return;
  }
  
  DynamicJsonDocument alarmsJson(3000);
  char resp[1024] {};

  JsonArray alarms = alarmsJson.createNestedArray("alarms");
  
  const mAlarmList& list = _storage.getAlarms();
  for(int i = 0; i < 8; ++i)
  {
    if(list.arr[i].hour != -1)
    {
      JsonObject alarm = alarms.createNestedObject();
      alarm["id"] = i;
      alarm["hour"] = list.arr[i].hour;
      alarm["minute"] = list.arr[i].minute;

      JsonArray dows = alarm.createNestedArray("dows");
      for(int j = 0; j < 7; ++j)
      {
        if(list.arr[i].dows[j])
        {
          dows.add(j);
        }
      }
 
      alarm["isEnabled"] = list.arr[i].isEnabled;
    }  
  }

  serializeJson(alarmsJson, resp);
  strcpy(cachedAlarmResp, resp);
  server.send(200, F("application/json"), resp); //HTTP_OK
}


void deleteAllAlarms()
{
  alarmsChanged = true;
  _storage.clearMem();  
  server.send(204); //HTTP_NO_CONTENT
}


// -----------------------
// --- GET '/datetime' ---
// -----------------------
void getDateTime() {  
  mTime tm = _clk.getTime();
  mDate dt = _clk.getDate();
  int dow = _clk.getDow();

  StaticJsonDocument<256> dateTimeJson;
  
  dateTimeJson["second"] = tm.second;
  dateTimeJson["minute"] = tm.minute;
  dateTimeJson["hour"] = tm.hour;
  dateTimeJson["day"] = dt.day;
  dateTimeJson["month"] = dt.month;
  dateTimeJson["year"] = dt.year;
  dateTimeJson["dow"] = dow;


  char resp[256];
  serializeJson(dateTimeJson, resp);
  
  server.send(200, F("application/json"), resp);
}


// -----------------------
// --- PUT '/datetime' ---
// -----------------------
void setDateTime() {
  String body = server.arg("plain");

  Serial.println(body);
 
  StaticJsonDocument<256> req;
  DeserializationError error = deserializeJson(req, body);

  //validate request
  if((error.code() != DeserializationError::Ok) ||
     !(req.containsKey("second")) || !(req["second"].is<int>()) ||
     !(req.containsKey("minute")) || !(req["minute"].is<int>()) ||
     !(req.containsKey("hour"))   || !(req["hour"].is<int>())   ||
     !(req.containsKey("day"))    || !(req["day"].is<int>())    ||
     !(req.containsKey("month"))  || !(req["month"].is<int>())  ||
     !(req.containsKey("year"))   || !(req["year"].is<int>())   ||
     !(req.containsKey("dow"))    || !(req["dow"].is<int>()))
  {
    server.send(400); //BAD_REQUEST
    return;
  }

  
  mTime tm = mTime {
    req["second"].as<int>(),
    req["minute"].as<int>(),
    req["hour"].as<int>()
  };

  mDate dt = mDate {
    req["day"].as<int>(),
    req["month"].as<int>(),
    req["year"].as<int>()
  };

  int dow = req["dow"].as<int>();

  if(!(_clk.setDateDowTime(dt, dow, tm))) {
    server.send(400); //BAD_REQUEST
    return;
  }
  
  Serial.print(F("HTTP Method: "));
  Serial.println(server.method());

  server.send(204); //NO_CONTENT
}


// -----------------------------
// --- DELETE '/alarms/{id}' ---
// -----------------------------
void removeAlarm()
{
  int id = atoi(server.pathArg(0).c_str());
  bool found = _storage.existsById(id);
  if(!found)
  {
    server.send(404); //NOT_FOUND 
    return;
  }
  
  bool removed = _storage.removeAlarm(id);
  if(!removed) {
    server.send(400); //BAD_REQUEST
    return;
  }
  
  alarmsChanged = true;

  server.send(204); //NO_CONTENT
}


// -----------------------
// --- POST '/alarms' ----
// -----------------------
void postAlarm() 
{
  String body = server.arg("plain");
  Serial.println(body);
 
  StaticJsonDocument<256> req;
  DeserializationError error = deserializeJson(req, body);

  //validate request
  if((error.code() != DeserializationError::Ok) || 
     !(req.containsKey("hour"))   || !(req["hour"].is<int>()) ||
     !(req.containsKey("minute")) || !(req["minute"].is<int>()) ||
     !(req.containsKey("dows"))   || !(req["dows"].is<JsonArray>()))
  {
    server.send(400); //BAD_REQUEST
    return;
  }

  
  mAlarm alarm {
    req["hour"].as<int>(),
    req["minute"].as<int>()
  };

  alarm.isEnabled = (req.containsKey("isEnabled")) ? req["isEnabled"].as<bool>() : true;

  JsonArray dows = req["dows"].as<JsonArray>();
  
  bool validDows = true;

  for(auto d : dows) {
      if (!(d.is<int>())) {
        validDows = false;
        break;
      }

      int dval = d.as<int>();
      
      if(dval < 0 || dval > 6)
      {
        validDows = false;
        break;
      }
      alarm.dows[dval]= true;
  }

  if(!validDows || !(_storage.validateAlarm(alarm))) {
    server.send(400); //BAD_REQUEST
    return;
  }
  
  int id = _storage.saveAlarm(alarm);
  
  Serial.print(F("HTTP Method: "));
  Serial.println(server.method());

  if(id == -1) 
  {
    server.send(403); //FORBIDDEN = no free space avaliable
    return;
  }
  else 
  {
    StaticJsonDocument<128> idJson;
    idJson["id"] = id;
    
    char idResp[128];
    serializeJson(idJson, idResp);

    alarmsChanged = true;
    server.send(200, F("application/json"), idResp); 
  }
  
}


// ----------------------------
// --- PATCH '/alarms/{id}' ---
// ----------------------------
void editAlarm()
{
  String body = server.arg("plain");
  Serial.println(body);
 
  StaticJsonDocument<256> req;
  DeserializationError error = deserializeJson(req, body);

  //validate request
  if((error.code() != DeserializationError::Ok))
  {
    server.send(400); //BAD_REQUEST
    return;
  }


  int id = atoi(server.pathArg(0).c_str());
  bool found = _storage.existsById(id);
  if(!found)
  {
    server.send(404); //NOT_FOUND 
    return;
  }

  bool editIsEnabled = req.containsKey("isEnabled");
  bool editDows = req.containsKey("dows");
  bool editHour = req.containsKey("hour");
  bool editMinute = req.containsKey("minute");

  bool validIsEnabled = false;
  bool validDows = false;
  bool validHour = false;
  bool validMinute = false;
  
  if(editIsEnabled && (req["isEnabled"].is<bool>()))
  {
    bool enabled = req["isEnabled"];
    if(enabled) 
    {
      _storage.enableAlarm(id);
    }
    else
    {
      _storage.disableAlarm(id);  
    }

    validIsEnabled = true;
  }

  if(editDows && req["dows"].is<JsonArray>())
  {
    bool newDows[7] {};
    JsonArray dows = req["dows"].as<JsonArray>();

    bool transformDows = true;
    
    for(auto d : dows) {
        if (!(d.is<int>())) {
          transformDows = false;
          break;
        }
  
        int dval = d.as<int>();
        
        if(dval < 0 || dval > 6)
        {
          transformDows = false;
          break;
        }
        newDows[dval]= true;
    }
  
    if(transformDows)
    {
       _storage.changeAlarmDows(id, newDows);
       validDows = true;
    }
  }


  if(editHour && req["hour"].is<int>())
  {
    if(_storage.changeAlarmHour(id, req["hour"].as<int>()))
    {
        validHour = true;
    }
  }

  if(editMinute && req["minute"].is<int>())
  {
     if(_storage.changeAlarmMinute(id, req["minute"].as<int>()))
     {
        validMinute = true;
     }
  }


  
  StaticJsonDocument<256> patchJson;


  if(editMinute)
  {
    patchJson["minute"] = validMinute;
  }

  if(editHour)
  {
    patchJson["hour"] = validHour;
  }

  if(editDows)
  {
    patchJson["dows"] = validDows;
  }

  if(editIsEnabled)
  {
    patchJson["isEnabled"] = validIsEnabled;
  }


  char resp[256];
  serializeJson(patchJson, resp);
  
  server.send(200, F("application/json"), resp);
  
  alarmsChanged = true;
}



// =========== CLOCK PROGRAM ==========================================

// ------ DATA ------
enum states {
  STATE_ALARM_CHECK, STATE_ALARM_ACTION, STATE_SHOW_IP
};

enum states STATE, NEXT_STATE;

long alarmActionStartTime;


// ------ SETUP ------
void setup(void)
{
  Serial.begin(9600);
  Wire.begin();

  //RTC
  _clk.init();
  
  //VIEW
  lcd.init();                     
  lcd.backlight();
  _view.init();

  //TEMPERATURE
  _temperature.init();


  //STORAGE
  _storage.init();


  //WIFI & WEB
  connectWifi();

  pinMode(PIEZO_PIN, OUTPUT);
  pinMode(BTN_PIN, INPUT_PULLUP);
  STATE = STATE_ALARM_CHECK;
}


void handleView()
{
  mTime tm = _clk.getTime();
  mDate dt = _clk.getDate();
  int dow = _clk.getDow();
  float temp = _temperature.getTemperature();

  
  TimeView tmw {
    tm.second,
    tm.minute,
    tm.hour
  };

  DateView dtw {
    dow,
    dt.day,
    dt.month,
    dt.year
  };

  TemperatureView tempw{
    temp  
  };

  _view.showTime(tmw);
  _view.showDate(dtw);
  _view.showTemperature(tempw);
}

void showIp()
{
  if(checkWifi())
  {
     _view.show(WiFi.localIP().toString().c_str());
  }
  else
  {
     _view.show("Disconnected :(");
  }
}

void alarmAction(long period)
{
  if((period / 1000) % 2)
  {
    tone(PIEZO_PIN, 1000); // 1KHz sound signal  
  }
  else
  {
    noTone(PIEZO_PIN);
  }
}

void stopAlarm()
{
  noTone(PIEZO_PIN);
}
  
mAlarm curAlarm = {-1};
bool wasAlarmStopped = false;

bool handleAlarm()
{
  mTime tm = _clk.getTime();
  int dow = _clk.getDow();

  if(wasAlarmStopped && (curAlarm.hour == tm.hour && curAlarm.minute == tm.minute && curAlarm.dows[dow])) {
    return false;
  }

  
  const mAlarm& nextAlarm = _storage.getNextAlarm(tm.hour, tm.minute, dow); 

  
  if((nextAlarm.hour == tm.hour && nextAlarm.minute == tm.minute && nextAlarm.dows[dow])) {
    curAlarm = nextAlarm;
    return true;
  }

  return false;
}

bool btnClicked = false;
bool checkBtn()
{
  if(btnClicked)
  {
    bool btnUp = (digitalRead(BTN_PIN) == HIGH);
    if(btnUp)
    {
      btnClicked = false;
      return true;
    }
  }
  else
  {
    btnClicked = (digitalRead(BTN_PIN) == LOW);
  }
  return false;
}


void loop(void)
{
  startServer();
  wm.process(); 
  if(checkWifi())
  {
     MDNS.update();
     server.handleClient();
  }
 
  switch(STATE)
  {
    case STATE_ALARM_CHECK:
      {
        if(handleAlarm())
        {
          NEXT_STATE = STATE_ALARM_ACTION;  
          alarmActionStartTime = millis();
          wasAlarmStopped = false;
          btnClicked = false;
        }  
        else
        {
          if(checkBtn())
          {
            btnClicked = false;
            _view.clear();
            showIp();
            NEXT_STATE = STATE_SHOW_IP;
          }
          else
          {
            handleView();
            NEXT_STATE = STATE_ALARM_CHECK;
          } 
        }
      }
      break;
    case STATE_ALARM_ACTION:
      {
        alarmAction(millis() - alarmActionStartTime);
        NEXT_STATE = STATE_ALARM_ACTION;
        
        if((millis() - alarmActionStartTime > 60 * 1000) || checkBtn())
        {
          btnClicked = false;
          _storage.updateNext();
          NEXT_STATE = STATE_ALARM_CHECK;
          wasAlarmStopped = true;
          stopAlarm();
        }
      }
      break;
    case STATE_SHOW_IP:
      {
        NEXT_STATE = STATE_SHOW_IP;
        
        if(checkBtn())
        {
          btnClicked = false;
          _view.clear();
          NEXT_STATE = STATE_ALARM_CHECK;
        }
      }
      break;
  }

  STATE = NEXT_STATE;
}
