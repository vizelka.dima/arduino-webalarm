#pragma once

struct mTime 
{
  int second; // <0, 59>
  int minute; // <0, 59>
  int hour;   // <0, 23>
};


struct mDate 
{
  int day;   // <1, 31>
  int month; // <1, 12>
  int year;  // <2000, 2099>
};


class DateTimeInterface
{
  public:
    virtual void  init           ( ) { }
    virtual mTime getTime        ( ) = 0;  
    virtual mDate getDate        ( ) = 0;
    virtual int   getDow         ( ) = 0;
    virtual bool  setDateDowTime ( mDate newDate, 
                                   int newDow,
                                   mTime newTime ) = 0;
};
