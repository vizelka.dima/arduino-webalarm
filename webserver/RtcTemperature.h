#pragma once

#include <DS3231.h>
#include "TemperatureInterface.h"

class RtcTemperature : public TemperatureInterface 
{
  public:
    RtcTemperature(DS3231 & rtc) : _rtc(rtc){}
    
    float getTemperature();
    
  private:
    DS3231& _rtc; 
};
