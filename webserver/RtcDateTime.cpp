#include <DS3231.h>

#include "RtcDateTime.h"


mTime RtcDateTime::getTime()
{
  bool h12Flag = false;
  bool pmFlag = false;

  mTime curTime = mTime{
    this->_rtc.getSecond(),
    this->_rtc.getMinute(), 
    this->_rtc.getHour(h12Flag, pmFlag)
  };
                       
  return curTime;
}

 
mDate RtcDateTime::getDate()
{
  bool century = false;

  return mDate{
      this->_rtc.getDate(),
      this->_rtc.getMonth(century),
      this->_rtc.getYear() + 2000
  };
}

 
int RtcDateTime::getDow()
{
  return _rtc.getDoW() - 1; // 0 - 6
}

bool RtcDateTime::setDateDowTime(mDate newDate, 
                                 int newDow,
                                 mTime newTime)
{
  if((newDow < 0 || newDow > 6) ||
     (newTime.second < 0 || newTime.second > 59) ||
     (newTime.minute < 0 || newTime.minute > 59) ||
     (newTime.hour < 0   || newTime.hour > 23)   ||
     (newDate.day < 0    || newDate.day > 31)    ||
     (newDate.month < 1  || newDate.month > 12)  ||
     (newDate.year < 2000))
  {
    return false;
  }
  
  //day of week
  _rtc.setDoW(newDow + 1);

  //date
  _rtc.setDate(newDate.day);    //in DS3231.h date means day :)
  _rtc.setMonth(newDate.month);
  _rtc.setYear(newDate.year - 2000);

  //time
  _rtc.setHour(newTime.hour);
  _rtc.setMinute(newTime.minute);
  _rtc.setSecond(newTime.second);

  return true;
}
